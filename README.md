# GitLabPages for Hugo

Deploy a static website on GitLab Pages built using [Hugo](https://gohugo.io/) framework.

## Usage

```yaml
include:
  - component: gitlab.com/gitlab-test-ci-catalog/catalog/pages-hugo@main

stages:
  - test # ensure at least the `test` stage is defined.
```

This component adds 2 conditional jobs:
1. A `pages` job in the `test` stage, when the pipeline runs for the default branch.
1. A `test` job in the `test` stage, when the pipeline does not run for the default branch.